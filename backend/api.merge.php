<?php

include("lib/api_betaface.lib.php");
include("lib/api_faceplusplus.lib.php");

//json header
header('Content-Type: application/json');

//Betaface
/** 
* Use Public api key and secret
**/

$api_key_b = 'd45fd466-51e2-4701-8da8-04351c872236';
$api_secret_b = '171e8465-f548-401d-b63b-caf0dc28df5f';

$image = 'example/test2.jpg';

$upload_return_b = upload_image($api_key_b, $image);
$upload_return_b = json_decode($upload_return_b, true);
//print_r($upload_return);


$fetch_return_b = get_image_info($api_key_b,$upload_return_b["media"]["faces"][0]["face_uuid"]);
$fetch_return_b = json_decode($fetch_return_b, true);

//faceplusplus
/** 
* Use Public api key and secret
**/

$api_key = '1rsj0WDsGkaMg6CCV6VW1zAmk77aW8b2';
$api_secret = 'G4TLB7MXzl5QzMzcQfTrwl9AVgWEUT8j';

$upload_return = upload_detect_image_plusplus($api_key, $api_secret, "gender,age,smiling,headpose,facequality,blur,eyestatus,emotion,ethnicity,beauty,mouthstatus,eyegaze,skinstatus", $image);
$fetch_return = json_decode($upload_return, true);

//Output part

//Right eye
//eye open

	if($fetch_return["faces"][0]["attributes"]["eyestatus"]["right_eye_status"]["no_glass_eye_open"] > $fetch_return["faces"][0]["attributes"]["eyestatus"]["right_eye_status"]["normal_glass_eye_open"]){
		$temp["eyeopen"] = $fetch_return["faces"][0]["attributes"]["eyestatus"]["right_eye_status"]["no_glass_eye_open"];
	}
	else{
		$temp["eyeopen"] = $fetch_return["faces"][0]["attributes"]["eyestatus"]["right_eye_status"]["normal_glass_eye_open"];
	}
//eyeclose
	if($fetch_return["faces"][0]["attributes"]["eyestatus"]["right_eye_status"]["no_glass_eye_close"] > $fetch_return["faces"][0]["attributes"]["eyestatus"]["right_eye_status"]["normal_glass_eye_close"]){
		$temp["eyeclose"] = $fetch_return["faces"][0]["attributes"]["eyestatus"]["right_eye_status"]["no_glass_eye_close"];
	}
	else{
		$temp["eyeclose"] = $fetch_return["faces"][0]["attributes"]["eyestatus"]["right_eye_status"]["normal_glass_eye_close"];
	}
//eyeopen or close
	if($temp["eyeopen"] >= $temp["eyeclose"]){
		$output["right_eye"] = 1;
	}
	else{
		$output["right_eye"] = 0;
	}

//Left eye
//eye open

	if($fetch_return["faces"][0]["attributes"]["eyestatus"]["left_eye_status"]["no_glass_eye_open"] > $fetch_return["faces"][0]["attributes"]["eyestatus"]["left_eye_status"]["normal_glass_eye_open"]){
		$temp["eyeopen"] = $fetch_return["faces"][0]["attributes"]["eyestatus"]["left_eye_status"]["no_glass_eye_open"];
	}
	else{
		$temp["eyeopen"] = $fetch_return["faces"][0]["attributes"]["eyestatus"]["left_eye_status"]["normal_glass_eye_open"];
	}
//eyeclose
	if($fetch_return["faces"][0]["attributes"]["eyestatus"]["left_eye_status"]["no_glass_eye_close"] > $fetch_return["faces"][0]["attributes"]["eyestatus"]["left_eye_status"]["normal_glass_eye_close"]){
		$temp["eyeclose"] = $fetch_return["faces"][0]["attributes"]["eyestatus"]["left_eye_status"]["no_glass_eye_close"];
	}
	else{
		$temp["eyeclose"] = $fetch_return["faces"][0]["attributes"]["eyestatus"]["left_eye_status"]["normal_glass_eye_close"];
	}
//eyeopen or close
	if($temp["eyeopen"] >= $temp["eyeclose"]){
		$output["left_eye"] = 1;
	}
	else{
		$output["left_eye"] = 0;
	}

//emotion
	$value = max($fetch_return["faces"][0]["attributes"]["emotion"]);
	$output["emotion"] = array_search($value, $fetch_return["faces"][0]["attributes"]["emotion"]);

//glasses
	if($fetch_return["faces"][0]["attributes"]["glass"]["value"]=="NONE"){
		$output["glass"] = 0;
	}
	else{
		$output["glass"] = 1;
	}

//gender
	$output["gender"] = $fetch_return["faces"][0]["attributes"]["gender"]["value"];

//hair
	if($fetch_return_b["tags"][10]["value"]=="yes"){
		$output["hair"]["color"] = "Black";
	}
	else if($fetch_return_b["tags"][11]["value"]=="yes"){
		$output["hair"]["color"] = "Blond";
	}
	else if($fetch_return_b["tags"][13]["value"]=="yes"){
		$output["hair"]["color"] = "Brown";
	}
	else if($fetch_return_b["tags"][21]["value"]=="yes"){
		$output["hair"]["color"] = "Gray";
	}
	else{
		$output["hair"]["color"] = "Unknow";	
	}

	if($fetch_return_b["tags"][35]["value"]=="yes"){
		$output["hair"]["style"] = "Straight";	
	}
	else if($fetch_return_b["tags"][36]["value"]=="yes"){
		$output["hair"]["style"] = "wavy";	
	}

//oval face
	if($fetch_return_b["tags"][27]["value"]=="yes"){
		$output["oval_face"] = 1;
	}
	else{
		$output["oval_face"] = 0;
	}

echo json_encode($output, JSON_PRETTY_PRINT);