<?php
	/* https://www.betafaceapi.com/wpa/index.php/documentation */
	
	function upload_image($api_key, $file){
		$ch = curl_init();
		$upload = base64_encode(file_get_contents($file));
		//$headers = array('accept' => 'application/json','Content-type' => 'application/json');
		$data = array("api_key"=>$api_key, "file_base64"=>$upload);
		$data = json_encode($data);
		curl_setopt($ch, CURLOPT_URL, 'https://www.betafaceapi.com/api/v2/media');
		curl_setopt($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_HTTPHEADER , $headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(  
			'accept: application/json',
   			'Content-Type: application/json',  
   			'Content-Length: ' . strlen($data))  
 		);  
		$content  = curl_exec($ch);
		curl_close($ch);
		return $content;
	}

	function get_image_info($api_key, $image_uuid){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,'https://www.betafaceapi.com/api/v2/face?api_key='.$api_key.'&face_uuid='.$image_uuid);
		$content = curl_exec($ch);
		curl_close($ch);
		return $content;
	}