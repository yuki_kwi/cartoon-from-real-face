<?php
function merge_png($file){
	$final_img = imagecreatefrompng($file[0]);
	
	$len = count($file);
	$i = 1;
	while($i<$len){
		$image[$i] = imagecreatefrompng($file[$i]);
		$i++;
	}

	imagealphablending($final_img, true);
	imagesavealpha($final_img, true);
	
	$i = 1;
	while($i<$len){
		$data = getimagesize($file[$i]);
		$width = $data[0];
		$height = $data[1];
		imagecopy($final_img, $image[$i], 0, 0, 0, 0, $width, $height);
		$i++;
	}
	return $final_img;
}

header('Content-Type: image/png');

$data = file_get_contents('image_list.json');
$data = json_decode($data, true);

if($_GET["gender"]=="male"){
	if(isset($_GET["glasses"])){
		//Example URL : lib.merge_png.php?gender=male&body=1&shirt=black&face_type=male_normal&face_color=normal&hair_type=1&hair=red&eye_type=left-close-smile&eye=1&brow_type=1&brow=1&nose=1&mouth=1&glasses=1
		$img = merge_png(
			array(
				"image/male/body/".$data["male"]["body"][$_GET["body"]],
				"image/male/shirt/".$data["shirt"][$_GET["shirt"]],
				"image/face/".$_GET["face_type"]."/".$data["face"][$_GET["face_type"]][$_GET["face_color"]],
				"image/".$_GET["gender"]."/hair/".$_GET["hair_type"]."/".$data[$_GET["gender"]]["hair"][$_GET["hair_type"]][$_GET["hair"]],
				"image/eye/".$_GET["eye_type"]."/".$data["eye"][$_GET["eye_type"]][$_GET["eye"]]."",
				"image/brow/".$_GET["brow_type"]."/".$data["brow"][$_GET["brow_type"]][$_GET["brow"]]."",
				"image/nose/".$data["nose"][$_GET["nose"]],
				"image/mouth/".$data["mouth"][$_GET["mouth"]]."",
				"image/glasses/".$data["glasses"][$_GET["glasses"]]
			)
		);
	}
	else{
		//Example URL : lib.merge_png.php?gender=male&body=1&shirt=black&face_type=male_normal&face_color=normal&hair_type=1&hair=red&eye_type=left-close-smile&eye=1&brow_type=1&brow=1&nose=1&mouth=1
		$img = merge_png(
			array(
				"image/male/body/".$data["male"]["body"][$_GET["body"]],
				"image/male/shirt/".$data["shirt"][$_GET["shirt"]],
				"image/face/".$_GET["face_type"]."/".$data["face"][$_GET["face_type"]][$_GET["face_color"]],
				"image/".$_GET["gender"]."/hair/".$_GET["hair_type"]."/".$data[$_GET["gender"]]["hair"][$_GET["hair_type"]][$_GET["hair"]],
				"image/eye/".$_GET["eye_type"]."/".$data["eye"][$_GET["eye_type"]][$_GET["eye"]]."",
				"image/brow/".$_GET["brow_type"]."/".$data["brow"][$_GET["brow_type"]][$_GET["brow"]]."",
				"image/nose/".$data["nose"][$_GET["nose"]],
				"image/mouth/".$data["mouth"][$_GET["mouth"]].""
			)
		);
	}
}
if($_GET["gender"]=="female"){
	if(isset($_GET["glasses"])){
		//Example URL : lib.merge_png.php?gender=female&body=1&shirt=black&face_type=fat&face_color=pink&hair_type=horse&hair=red&eye_type=left-close-smile&eye=1&brow=1&brow_type=1&nose=1&mouth=2&glasses=1
		$img = merge_png(
			array(
				"image/".$_GET["gender"]."/hair/".$_GET["hair_type"]."/back/".$data[$_GET["gender"]]["hair"][$_GET["hair_type"]][$_GET["hair"]],
				"image/female/body/".$data["female"]["body"][$_GET["body"]],
				"image/female/shirt/".$data["shirt"][$_GET["shirt"]],
				"image/face/".$_GET["face_type"]."/".$data["face"][$_GET["face_type"]][$_GET["face_color"]],
				"image/".$_GET["gender"]."/hair/".$_GET["hair_type"]."/front/".$data[$_GET["gender"]]["hair"][$_GET["hair_type"]][$_GET["hair"]],
				"image/eye/".$_GET["eye_type"]."/".$data["eye"][$_GET["eye_type"]][$_GET["eye"]]."",
				"image/brow/".$_GET["brow_type"]."/".$data["brow"][$_GET["brow_type"]][$_GET["brow"]]."",
				"image/nose/".$data["nose"][$_GET["nose"]],
				"image/mouth/".$data["mouth"][$_GET["mouth"]]."",
				"image/glasses/".$data["glasses"][$_GET["glasses"]]
			)
		);
	}
	else{
		//Example URL : lib.merge_png.php?gender=female&body=1&shirt=black&face_type=fat&face_color=pink&hair_type=horse&hair=red&eye_type=left-close-smile&eye=1&brow=1&brow_type=1&nose=1&mouth=2
		$img = merge_png(
			array(
				"image/".$_GET["gender"]."/hair/".$_GET["hair_type"]."/back/".$data[$_GET["gender"]]["hair"][$_GET["hair_type"]][$_GET["hair"]],
				"image/female/body/".$data["female"]["body"][$_GET["body"]],
				"image/female/shirt/".$data["shirt"][$_GET["shirt"]],
				"image/face/".$_GET["face_type"]."/".$data["face"][$_GET["face_type"]][$_GET["face_color"]],
				"image/".$_GET["gender"]."/hair/".$_GET["hair_type"]."/front/".$data[$_GET["gender"]]["hair"][$_GET["hair_type"]][$_GET["hair"]],
				"image/eye/".$_GET["eye_type"]."/".$data["eye"][$_GET["eye_type"]][$_GET["eye"]]."",
				"image/brow/".$_GET["brow_type"]."/".$data["brow"][$_GET["brow_type"]][$_GET["brow"]]."",
				"image/nose/".$data["nose"][$_GET["nose"]],
				"image/mouth/".$data["mouth"][$_GET["mouth"]].""
			)
		);
	}
}
imagepng($img);
//$img = merge_png(array("ผมด้านหลัง-01.png","ลำตัว-01.png","เสื้อ-01.png","ใบหน้ารูปปกติ-01.png","ผมด้านหน้า-01.png")); //Usage example


