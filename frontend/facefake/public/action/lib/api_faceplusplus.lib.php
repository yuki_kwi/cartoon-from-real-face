<?php
    /* https://www.faceplusplus.com/ */
    function upload_detect_image_plusplus($api_key, $api_secret, $return_attributes, $file){
		$ch = curl_init();
		$upload = base64_encode(file_get_contents($file));
		$data = array('api_key' => $api_key,'api_secret' => $api_secret, 'image_base64' => $upload, 'return_attributes' => $return_attributes);
		curl_setopt($ch, CURLOPT_URL, 'https://api-us.faceplusplus.com/facepp/v3/detect');
		curl_setopt($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_HTTPHEADER , $headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$content  = curl_exec($ch);
		curl_close($ch);
		return $content;

	}    