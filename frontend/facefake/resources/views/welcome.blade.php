<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/main.css') }}">
    <title>MAKEFACE</title>
</head>
<body>

<div id="app">

<nav id="navbar" class="navbar navbar-expand-sm fixed-top navbar-light" style="background-color: #fff;">
<div class="container">
    <div class="navbar-header">
        <a class="navbar-brand" href="#"><i class="mdi mdi-face"></i>&nbsp;MAKEFACE</a>
    </div>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="#"><i class="mdi mdi-home"></i>&nbsp;หน้าหลัก</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#about"><i class="mdi mdi-information"></i>&nbsp;เกี่ยวกับเรา</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#enterin"><i class="mdi mdi-creation"></i>&nbsp;สร้างตัวการ์ตูน</a>
        </li>
      </ul>
    </div>
</div>
</nav>

<section class="header">
<div id="search-overlay"></div>
<div id="demo" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  
  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active banner">
      <div id="banner-overlay"></div>
      <img class="banner-image" src="{{ asset('img/banner-01.jpg') }}" alt="Los Angeles">
      <div class="banner-details fadeIn-top">
        <i class="mdi mdi-apple-mobileme" style="color: #fff;"></i>
        <h1>สร้างตัวละครของคุณเอง</h1>
        <p>This is a short description</p>
      </div>
    </div>
    <div class="carousel-item">
      <div id="banner-overlay"></div>
      <img class="banner-image" src="{{ asset('img/banner-01.jpg') }}" alt="Los Angeles">
      <div class="banner-details fadeIn-top">
        <i class="mdi mdi-map-marker mdi-48px" style="color: #fff;"></i>
        <h1>ปรับเเต่งสไตล์ที่ใช่</h1>
        <p>This is a short description</p>
      </div>
    </div>
    <div class="carousel-item">
      <div id="banner-overlay"></div>
      <img class="banner-image" src="{{ asset('img/banner-01.jpg') }}" alt="Los Angeles">
      <div class="banner-details fadeIn-top">
        <i class="mdi mdi-account-multiple mdi-48px" style="color: #fff;"></i>
        <h1>เเบ่งปันผลงานของคุณกับเพื่อน</h1>
        <p>This is a short description</p>
      </div>
    </div>
  </div>
  
  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
</section>


<section id="about" style="text-align:center; padding: 5rem; background-color:#FFF; color:#fff;">
<h1 style="margin-bottom:30px;color: #111"><i class="mdi mdi-information"></i>&nbsp;จะสร้างตัวละครได้อย่างไร</h1>
<p style="color:#000;">ลองให้คอมพิวเตอร์ออกเเบบตัวละครของคุณดูสิ</p>
<center>
    <div class="about row" style="margin-top: 100px;" >
        <div class="about col-sm-4">
            <div class="content">
              <div class="content-overlay"></div>
              <img class="content-image" src="img/import.gif">
              <div class="content-details fadeIn-top">
                <i class="mdi mdi-camera mdi-48px"></i>
                <h3>นำเข้ารูปภาพ</h3>
                <p>จากไฟล์ในอุปกรณ์ของคุณ</p>
              </div>
            </div>
        </div>
        <div class="about col-sm-4">
            <div class="content">
              <div class="content-overlay"></div>
              <img class="content-image" src="img/face.gif">
              <div class="content-details fadeIn-top">
                <i class="mdi mdi-sync mdi-48px"></i>
                <h3>ประมวลผลด้วย AI</h3>
                <p>ใช้ betafaceapi & faceplusplus</p>
              </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="content">
              <div class="content-overlay"></div>
              <img class="content-image" src="img/edit.gif">
              <div class="content-details fadeIn-top">
                <i class="mdi mdi-pen mdi-48px"></i>
                <h3>ปรับเเต่งใหม่</h3>
                <p>เเก้ไขตัวการ์ตูนให้เป็นตัวตนของคุณ</p>
              </div>
            </div>
        </div>
    </div>
</center>
</section>

<section id="enterin">
  <center>
    <button onclick="location.href='{{ url('makeface') }}'" type="button" class="btn btn-outline-primary btn-lg">Start Create!</button>
  </center>
</section>

<footer style="vertical-align: text-bottom;">
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <h3>Font End</h3><br>
        <a href="">Sublime Text 3</a><br>
        <a href="">Bootstap 4</a><br>
      </div>
      <div class="col-sm-3">
        <h3>Back End</h3><br>
        <a href="">php</a><br>
        <a href="http://betafaceapi.com/">betafaceapi</a>
      </div>
      <div class="col-sm-3"></div>
      <div class="col-sm-3">
        <h3>Develope by</h3><br>
        <a href="">Non</a><br>
        <a href="">Towrung</a><br>
        <a href="">Oum</a><br>
        <a href="">Lisa</a><br>
      </div>
    </div>
  </div>
</footer>

</div>

<script type="text/javascript" src="{{ asset('/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $("#register-link").click(()=>{
        $("#SignInModal").modal("hide")
    })
    window.onscroll = function() {scrollFunction()};
    function scrollFunction() {
      if (document.body.scrollTop > 300 || document.documentElement.scrollTop > 300) {
        document.getElementById("navbar").style.background = "#000";
        document.getElementById("navbar").style.padding = "0.5rem 1rem";
        document.getElementById("navbar").classList.remove("navbar-light");
        document.getElementById("navbar").classList.add("navbar-dark");
      } else {
        document.getElementById("navbar").style.padding = "0.5rem 1rem";
        document.getElementById("navbar").style.background = "#fff";
        document.getElementById("navbar").classList.remove("navbar-dark");
        document.getElementById("navbar").classList.add("navbar-light");
      }
    } 
</script>
<script type="text/javascript">
    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
          && 
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000, function() {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) { // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              };
            });
          }
        }
      });
</script>
</body>
</html>