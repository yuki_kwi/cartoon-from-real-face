<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/upload.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/init.css') }}">
    <link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/nice.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/progressbar.css') }}">
    <title>MAKEFACE</title>
</head>

<body>

    <div id="app">
        <div id="loading">
            <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            <br><br>
            <center>
                <div class="progress" style="width: 20%;height: 30px;">
                    <div class="progress-bar progress-bar-success bg-success myprogress" role="progressbar" style="width:0%;font-weight: bold">0%</div>
                </div>
            </center>
        </div>

        <nav id="navbar" class="navbar navbar-expand-sm fixed-top navbar-light" style="background-color: #fff;">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/"><i class="mdi mdi-face"></i>&nbsp;MAKEFACE</a>
                </div>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="mdi mdi-home"></i>&nbsp;หน้าหลัก</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#about"><i class="mdi mdi-information"></i>&nbsp;เกี่ยวกับเรา</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#enterin"><i class="mdi mdi-creation"></i>&nbsp;สร้างตัวการ์ตูน</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Multi step form -->
        <section class="multi_step_form">
            <form id="msform">
                <!-- Tittle -->
                <div class="tittle">
                    <h2>เริ่มขั้นตอนการทำงาน</h2>
                    <p>In order to use this service, you have to complete this verification process</p>
                </div>
                <!-- progressbar -->
                <ul id="progressbar">
                    <li class="active">Upload Photo</li>
                    <li>Edit Picture</li>
                    <li>Download</li>
                </ul>
                <!-- fieldsets -->
                <fieldset>
                    <h3>Upload your photo</h3>
                    <h6>We will analyze your Photo & Assemble a picture for you.</h6>
                    <div class="avatar-upload">
                        <div class="avatar-edit">
                            <input type='file' id="imageFile" accept=".png, .jpg, .jpeg" />
                            <label for="imageFile"></label>
                        </div>
                        <div class="avatar-preview">
                            <div id="imagePreview" style="background-image: url('img/profile.png');">
                            </div>
                        </div>
                    </div>
                    <button type="button" class="action-button previous_button">Back</button>
                    <button type="button" id="upload_button" class="next action-button">Continue</button>
                </fieldset>

                <fieldset>
                    <h3>Cartoon Pictures</h3>
                    <h6>Image after detected and convert to cartoon</h6><br>
                    <select class="form-control" id="change_hair">
                        <option value="0">เลือกสีผม</option>
                        <option value="black">ดำ</option>
                        <option value="red">แดง</option>
                        <option value="grey">เทา</option>
                    </select>
                    <img src="" id="img">
                    <button type="button" class="action-button previous_button" style="margin-top:50px;">Back</button>
                    <button type="button" class="next action-button">Continue</button>
                </fieldset>

                <fieldset>
                    <h3>Download Pictures</h3>
                    <h6>Save your Cartoon Picture & Share with your friends</h6>
                    <div class="form-group">
                        <select class="product_select">
                            <option data-display="1. Choose A Question">1. Choose A Question</option>
                            <option>2. Choose A Question</option>
                            <option>3. Choose A Question</option>
                        </select>
                    </div>
                    <div class="form-group fg_2">
                        <input type="text" class="form-control" placeholder="Anwser here:">
                    </div>
                    <div class="form-group">
                        <select class="product_select">
                            <option data-display="1. Choose A Question">1. Choose A Question</option>
                            <option>2. Choose A Question</option>
                            <option>3. Choose A Question</option>
                        </select>
                    </div>
                    <div class="form-group fg_3">
                        <input type="text" class="form-control" placeholder="Anwser here:">
                    </div>
                    <button type="button" class="action-button previous previous_button">Back</button>
                    <a href="#" class="action-button">Save</a>
                </fieldset>
            </form>
        </section>
        <!-- End Multi step form -->

    </div>

    <script type="text/javascript" src="{{ asset('/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/init.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/nice.js') }}"></script>
    <!-- Upload Image -->
    <script>
        $(function () {
            $("#change_hair").change(()=>{
                $("#img").attr('src', 'action/lib/lib.merge_png.php?gender=male&body=1&shirt=black&face_type=male_normal&face_color=normal&hair_type=1&hair='+ $("#change_hair").val().toLowerCase() +'&eye_type=right-close-smile&eye=1&brow_type=1&brow=1&nose=1&mouth=1')
            })
            $('#upload_button').click(function () {
                $("#loading").css("display", "block");
                $('.myprogress').css('width', '0');
                var formData = new FormData();
                var files = $('#imageFile')[0].files[0];
                formData.append('file',files);
                $.ajax({
                    url: '/action/api.merge.php',
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    // this part is progress bar
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('.myprogress').text(percentComplete + '%');
                                $('.myprogress').css('width', percentComplete + '%');
                            }
                        }, false);
                        return xhr;
                    },
                    success: function (data) {
                        // lib.merge_png.php?gender=female&body=1&shirt=black&face_type=fat&face_color=pink&hair_type=horse&hair=red&eye_type=left-close-smile&eye=1&brow=1&brow_type=1&nose=1&mouth=2
                        if(data.gender.toLowerCase() == 'male'){
                            $("#img").attr('src', 'action/lib/lib.merge_png.php?gender=male&body=1&shirt=black&face_type=male_normal&face_color=normal&hair_type=1&hair='+ data.hair.color.toLowerCase() +'&eye_type=right-close-smile&eye=1&brow_type=1&brow=1&nose=1&mouth=1')
                        }else{
                            $("#img").attr('src', 'action/lib/lib.merge_png.php?gender=female&body=1&shirt=black&face_type=fat&face_color=pink&hair_type=horse&hair='+ data.hair.color.toLowerCase() +'&eye_type=left-close-smile&eye=1&brow=1&brow_type=1&nose=1&mouth=2&glasses=1')
                        }
                        $("#loading").css("display", "none");
                    }
                });
            });
        });
    </script>
    <!--  -->
    <script type="text/javascript">
        $("#register-link").click(() => {
            $("#SignInModal").modal("hide")
        })
        window.onscroll = function() {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 300 || document.documentElement.scrollTop > 300) {
                document.getElementById("navbar").style.background = "#000";
                document.getElementById("navbar").style.padding = "0.5rem 1rem";
                document.getElementById("navbar").classList.remove("navbar-light");
                document.getElementById("navbar").classList.add("navbar-dark");
            } else {
                document.getElementById("navbar").style.padding = "0.5rem 1rem";
                document.getElementById("navbar").style.background = "#fff";
                document.getElementById("navbar").classList.remove("navbar-dark");
                document.getElementById("navbar").classList.add("navbar-light");
            }
        }
    </script>
    <script type="text/javascript">
        $('a[href*="#"]')
            // Remove links that don't actually link to anything
            .not('[href="#"]')
            .not('[href="#0"]')
            .click(function(event) {
                // On-page links
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000, function() {
                            // Callback after animation
                            // Must change focus!
                            var $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            };
                        });
                    }
                }
            });
    </script>

    <!-- upload photo -->
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageFile").change(function() {
            readURL(this);
        });
    </script>

    <!-- progressbar -->
    <script type="text/javascript">
        ;
        (function($) {
            "use strict";

            //* Form js
            function verificationForm() {
                //jQuery time
                var current_fs, next_fs, previous_fs; //fieldsets
                var left, opacity, scale; //fieldset properties which we will animate
                var animating; //flag to prevent quick multi-click glitches

                $(".next").click(function() {
                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //activate next step on progressbar using the index of next_fs
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function(now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50) + "%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'transform': 'scale(' + scale + ')',
                                'position': 'absolute'
                            });
                            next_fs.css({
                                'left': left,
                                'opacity': opacity
                            });
                        },
                        duration: 800,
                        complete: function() {
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                });

                $(".previous").click(function() {
                    if (animating) return false;
                    animating = true;

                    current_fs = $(this).parent();
                    previous_fs = $(this).parent().prev();

                    //de-activate current step on progressbar
                    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                    //show the previous fieldset
                    previous_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function(now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale previous_fs from 80% to 100%
                            scale = 0.8 + (1 - now) * 0.2;
                            //2. take current_fs to the right(50%) - from 0%
                            left = ((1 - now) * 50) + "%";
                            //3. increase opacity of previous_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'left': left
                            });
                            previous_fs.css({
                                'transform': 'scale(' + scale + ')',
                                'opacity': opacity
                            });
                        },
                        duration: 800,
                        complete: function() {
                            animating = false;
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                });

                $(".submit").click(function() {
                    return false;
                })
            };

            //* Add Phone no select
            function phoneNoselect() {
                if ($('#msform').length) {
                    $("#phone").intlTelInput();
                    $("#phone").intlTelInput("setNumber", "+880");
                };
            };
            //* Select js
            function nice_Select() {
                if ($('.product_select').length) {
                    $('select').niceSelect();
                };
            };
            /*Function Calls*/
            verificationForm();
            phoneNoselect();
            nice_Select();
        })(jQuery);
    </script>

</body>

</html>